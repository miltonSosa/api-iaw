import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Evento,
  Option,
} from '../models';
import {EventoRepository} from '../repositories';

export class EventoOptionController {
  constructor(
    @repository(EventoRepository) protected eventoRepository: EventoRepository,
  ) { }

  @get('/eventos/{id}/options', {
    responses: {
      '200': {
        description: 'Array of Evento has many Option',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Option)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Option>,
  ): Promise<Option[]> {
    return this.eventoRepository.options(id).find(filter);
  }

  @post('/eventos/{id}/options', {
    responses: {
      '200': {
        description: 'Evento model instance',
        content: {'application/json': {schema: getModelSchemaRef(Option)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Evento.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Option, {
            title: 'NewOptionInEvento',
            exclude: ['id'],
            optional: ['eventoId']
          }),
        },
      },
    }) option: Omit<Option, 'id'>,
  ): Promise<Option> {
    return this.eventoRepository.options(id).create(option);
  }

  @patch('/eventos/{id}/options', {
    responses: {
      '200': {
        description: 'Evento.Option PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Option, {partial: true}),
        },
      },
    })
    option: Partial<Option>,
    @param.query.object('where', getWhereSchemaFor(Option)) where?: Where<Option>,
  ): Promise<Count> {
    return this.eventoRepository.options(id).patch(option, where);
  }

  @del('/eventos/{id}/options', {
    responses: {
      '200': {
        description: 'Evento.Option DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Option)) where?: Where<Option>,
  ): Promise<Count> {
    return this.eventoRepository.options(id).delete(where);
  }
}
