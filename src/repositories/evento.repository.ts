import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Evento, EventoRelations, Option} from '../models';
import {OptionRepository} from './option.repository';

export class EventoRepository extends DefaultCrudRepository < Evento, typeof Evento.prototype.id, EventoRelations > {
    public readonly options : HasManyRepositoryFactory < Option,
    typeof Evento.prototype.id >;
    constructor( @inject( 'datasources.mongodb' )dataSource : MongodbDataSource, @repository.getter( 'OptionRepository' )protected optionRepositoryGetter : Getter < OptionRepository >, )
    {
        super( Evento, dataSource );
        this.options = this.createHasManyRepositoryFactoryFor( 'options', optionRepositoryGetter, );
        this.registerInclusionResolver( 'options', this.options.inclusionResolver );
    }

}
