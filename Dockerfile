# Check out https://hub.docker.com/_/node to select a new base image
FROM node:10-slim AS loopback

# Set to a non-root built-in user `node`

# Create app directory (with user `node`)
RUN npm install -g @loopback/cli;

RUN mkdir -p /srv/app

WORKDIR /srv/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# COPY --chown=node package*.json ./
COPY package*.json ./

COPY . ./

# RUN npm install

# Bundle app source code

# RUN npm run build

# Bind to all network interfaces so that it can be mapped to the host OS
ENV HOST=0.0.0.0 PORT=3000

EXPOSE 3000
# CMD [ "node", "." ]
CMD [ "npm", "start" ]

# COPY --chown=node:node
# USER node
